<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubric extends Model {

	//
	protected $table = 'rubrics';


	protected $fillable = ['name','purposeOfAssessment','project_id'];

}
