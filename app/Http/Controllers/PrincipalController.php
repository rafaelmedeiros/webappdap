<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Request;
use Auth;
use App\User;
use App\Project;
use App\Guest;

class PrincipalController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$user = Auth::user();
    			$user_projects  = $user->projects()->get();
    			return view('principal')->with('projects',$user_projects);
	
	}

}

