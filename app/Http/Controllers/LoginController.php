<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use Auth;
use App\User;
use App\Project;
use App\Guest;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\GuestRequest;

class LoginController extends Controller {

	public function login()
	{

		$email = Request::only('email');
		$senha = bcrypt(Request::only('password'));

		if(Auth::attempt($email,$senha)) {
            return view('principal');
        }

		return view('/auth/login');
	}

	public function signUp(SignUpRequest $data){

		User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'institution' => $data['institution'],
			'type' => $data['type'],
			'institutionCountry' => $data['institutionCountry'],
			'acronym' => $data['acronym']
		]);

		return view('/auth/login');
	}

	public function guest(GuestRequest $data){

		Guest::create([
			'name' => $data['name'],
			'email' => $data['email']
		]);

		return view('home');
	}

	public function guestPage(){


		return view('auth/guest');
	}

}
