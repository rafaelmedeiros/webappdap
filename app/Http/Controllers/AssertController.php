<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Input;
use Auth;
use \GuzzleHttp\Client;


class AssertController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}
	

	public function addRegisterAssert(Request $request){



		$a = $request->get('owner');
		$b = $request->get('latitude');
		$c = $request->get('longitude');
		$d = $request->get('height');
		$e = $request->get('status');
		$f = $request->get('description');
		$g = $request->get('profile');
		$h = $request->get('planting');
		$i = $request->get('address');

		 $client = new Client();
		        $res = $client->request('POST', 'http://127.0.0.1:3000/registration/register', [
		            'form_params' => [
		                'owner' => $a,
		                'latitude' => $b,
		                'longitude' => $c,
		                'height' => $d,
		                'status' => $e,
		                'description' => $f,
		                'profile' => $g,
		                'planting' => $h,
		                'address' => $i

		            ]
		        ]);
		        

		return redirect()->back()->with('Sucesso!', 'Cadastro realizado');
     

	
			
	}


	public function formAssert(){

			return view('assert');
	}

}
