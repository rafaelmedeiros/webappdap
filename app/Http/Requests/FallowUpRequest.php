<?php namespace App\Http\Requests;

use App\Http\Requests\Request;


class FallowUpRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
        'grade' => 'required',
        'done' => 'required',
		];
	}

	public function messages()
	{
  		return [

    	 'required' => 'O campo :attribute é obrigatório',
    	 'numeric' => 'O campo :attribute deve ser numérico',
  ];
}

}
