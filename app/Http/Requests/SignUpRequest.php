<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SignUpRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
        'name' => 'required|max:100',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
        'institutionCountry' => 'required',
        'type' => 'required',
		'institution' => 'required',
		'acronym' => 'required'
  
		];
	}

	public function messages()
	{
  		return [

    	 'required' => 'O campo :attribute é obrigatório',
    	 'numeric' => 'O campo :attribute deve ser numérico',
  ];
}

}
