<?php namespace App\Http\Requests;

use App\Http\Requests\Request;


class RubricRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
        'name' => 'required|max:100|min:5',
        'purposeOfAssessment' => 'required',
        'nameCriteria' => 'required|max:100',
        'bellowStandard' => 'required|max:255',
        'approachingStandard' => 'required|max:255',
        'meetingStandard' => 'required|max:255'

        
  
  
		];
	}

	public function messages()
	{
  		return [

    	 'required' => 'O campo :attribute é obrigatório',
    	 'numeric' => 'O campo :attribute deve ser numérico',
  ];
}

}
