<?php namespace App\Http\Requests;

use App\Http\Requests\Request;


class TeamRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
        'nameTeam' => 'required|max:100|min:5',
        'teamCoach' => 'required|min:3',
        'name' => 'required|max:100',
        'email' => 'required|max:255|unique:students'
        
  
  
		];
	}

	public function messages()
	{
  		return [

    	 'required' => 'O campo :attribute é obrigatório',
    	 'numeric' => 'O campo :attribute deve ser numérico',
  ];
}

}
