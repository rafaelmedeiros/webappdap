<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
        'title' => 'required|max:100|min:5',
        'duration' => 'required|max:100',
        'course' => 'required|min:3',
        'courseLevel' => 'required',
        'otherSubjectArea' => 'required',
		'drivenQuestion' => 'required',
		'entryEvent' => 'required',
		'competences' => 'required',
		'markingProductPublic' => 'required',
		'onsitePeopleFacilities' => 'required',
		'equipments' => 'required',
		'materials' => 'required',
		'reflectionMethods' => 'required',
		'notes' => 'required',
		'communityResources' => 'required'
  
  
		];
	}

	public function messages()
	{
  		return [

    	 'required' => 'O campo :attribute é obrigatório',
    	 'numeric' => 'O campo :attribute deve ser numérico',
  ];
}

}
