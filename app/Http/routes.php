<?php

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');
Route::get('principal', 'ProjectController@listar');
Route::get('designProject', 'ProjectController@form');
Route::get('assert', 'AssertController@formAssert');
Route::get('manageTeam/{id}', 'TeamController@form');
Route::get('rubric/{id}', 'RubricController@form');
Route::get('fallowUp/{id}', 'FallowUpController@form');
Route::get('guestPage','LoginController@guestPage');
Route::post('/login', 'LoginController@login');
Route::post('/signUp','LoginController@signUp');
Route::post('/guest','LoginController@guest');
Route::post('/project/new', 'ProjectController@addProject');
Route::post('/assert/new', 'AssertController@addRegisterAssert');
Route::get('/project/delete/{id}','ProjectController@destroy');
Route::get('/project/edit/{id}','ProjectController@edit');
Route::get('/project/view/{id}','ProjectController@view');
Route::post('/project/update/{id}','ProjectController@update');
Route::post('/team/new/{id}', 'TeamController@addTeam');
Route::post('/rubric/new/{id}', 'RubricController@addRubric');
Route::post('/fellowUp/newTeam/{id}', 'FallowUpController@addNotaTeam');
Route::post('/fellowUp/newStudent/{id}', 'FallowUpController@addNotaStudent');

Route::get('/learningGuide/{id}', 'ProductController@form');
Route::post('/product/update', 'ProductController@update');


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
