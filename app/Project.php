<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $table = 'projects';

	public function products(){
        return $this->hasMany('\App\Product'); //Project Model Name
    }

    public function teams(){
        return $this->hasMany('\App\Team'); //Project Model Name
    }

    

	protected $fillable = ['title','duration','status','course','courseLevel','otherSubjectArea','drivenQuestion','entryEvent','competences','markingProductPublic','onsitePeopleFacilities','equipments','materials','communityResources','reflectionMethods','notes','user_id'];

}
