<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {

	//

	protected $table = 'teams';

	 public function students(){
        return $this->hasMany('\App\Student'); //Project Model Name
    }

	protected $fillable = ['nameTeam','teamCoach','project_id'];

}
