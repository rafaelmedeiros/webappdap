<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			'pais' => 'required',
			'type' => 'required',
			'academic_name' => 'required',
			'academic_acronym' => 'required'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'id_institution' => $data['id_institution'],
			'password' => bcrypt($data['password']),
			'institution' => $data['academic_name'],
			'type' => $data['type'],
			'institutionCountry' => $data['pais'],
			'acronym' => $data['academic_acronym']
		]);
	}


}
