<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model {

	//



	//
	protected $table = 'criterias';


	protected $fillable = ['nameCriteria','bellowStandard','approachingStandard','meetingStandard','rubric_id'];
}
