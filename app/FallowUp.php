<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FallowUp extends Model {

	//
	protected $table = 'fallow_ups';


	protected $fillable = ['student_id','grade','product_id','done'];
}
