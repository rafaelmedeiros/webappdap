<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	//
	protected $table = 'products';


	protected $fillable = ['name','teamOrIndividual','dueDate','learningOutComes','checkpoints','learningStrategies','project_id'];

}
