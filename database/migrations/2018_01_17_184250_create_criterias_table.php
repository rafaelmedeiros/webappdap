<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('criterias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nameCriteria');
			$table->string('bellowStandard');
			$table->string('approachingStandard');
			$table->string('meetingStandard');
			$table->integer('rubric_id')->unsigned();
			$table->timestamps();
		});

		Schema::table('criterias', function($table) {
       		
       		$table->foreign('rubric_id')->references('id')->on('rubrics')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('criterias');
	}

}
