<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('teamOrIndividual');
			$table->string('dueDate');
			$table->string('learningOutComes');
			$table->string('checkpoints');
			$table->string('learningStrategies');
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});

		Schema::table('products', function($table) {
       		
       		$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
