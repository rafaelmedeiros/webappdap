<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFallowUpsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fallow_ups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('grade');
			$table->string('done');
			$table->integer('product_id')->unsigned();
			$table->integer('student_id')->unsigned();
			$table->timestamps();
		});

		Schema::table('fallow_ups', function($table) {
       		
       		$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
       
   			});

		Schema::table('fallow_ups', function($table) {
       		
       		$table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fallow_ups');
	}

}
