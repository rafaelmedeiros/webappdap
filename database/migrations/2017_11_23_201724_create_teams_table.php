<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nameTeam');
			$table->string('teamCoach');
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});

		Schema::table('teams', function($table) {
       		
       		$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
	}

}
