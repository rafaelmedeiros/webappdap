<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRubricsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rubrics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('purposeOfAssessment');
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});



		Schema::table('rubrics', function($table) {
       		
       		$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rubrics');
	}

}
