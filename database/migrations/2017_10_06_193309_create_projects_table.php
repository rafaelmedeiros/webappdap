<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function($table)
		{
			$table->increments('id');
			
			$table->string('title');
			$table->string('duration');
			$table->string('status');
			$table->string('course');
			$table->string('courseLevel');
			$table->string('otherSubjectArea');
			$table->string('drivenQuestion');
			$table->string('entryEvent');
			$table->string('competences');
			$table->string('markingProductPublic');
			$table->string('onsitePeopleFacilities');
			$table->string('equipments');
			$table->string('materials');
			$table->string('communityResources');
			$table->string('reflectionMethods');
			$table->string('notes');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});

		Schema::table('projects', function($table) {
       		
       		$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
       
   			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
