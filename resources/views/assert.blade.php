@extends('custom_template')


@section('content')


 @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row projectcontainer">
	<div class="col-sm-10 col-sm-offset-1 form-box">
		<div class="form-top">
			<h3 class="title">Cadastro do Agricultor</h3>

		</div> 
		<div class="form-bottom contact-form" >

			<form action="/assert/new" method="post">

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				
				<div class="form-group">
					<center><span class="divisoria"> <label class="titlediv">Informe os dados </label> </span> </center>		
					<input type="text" name="owner" class="form-control" value="" placeholder="Proprietário" />
				</div>

				<div class="form-group">

					<input type="text" name="address" class="form-control" value="" placeholder="Endereço"/>
				</div>

				<div class="form-group">

					<input type="number" name="latitude" class="form-control" value="" placeholder="Latitude"/>
				</div>

				<div class="form-group">

					<input type="number" name="longitude" class="form-control" value="" placeholder="Longitude"/>
				</div>

				<div class="form-group">

					<input type="text" name="height" class="form-control" value="" placeholder="Altura"/>
				</div>



				<select align="center" class="selectpicker btn btn-lg" data-live-search="true" name="status" value=""> 
						<option>Irá começar</option>
						<option>Está em progresso</option>
						<option>Completado</option>
					</select>
				
				<br></br>

				<div class="form-group" align="center">
					<br>
					<h4 align="center" style="font-style: bold;">Perfil do agricultor:  </h4>
					<div class="row" >
						<div class="col-sm-2">
							<label class="radio-inline"><input type="radio" name="profile" value="dono">Dono</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="radio" name="profile" value="dono">Familiar próximo</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="radio" name="profile" value="mieiro">Mieiro</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="radio" name="profile" value="arrendador">Arrendador</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="radio" name="profile" value="trabalhador">Trabalhador</label>
						</div>
						
					</div>		
				</div>


				<div class="form-group" align="center">
					<br>
					<h4 align="center" style="font-style: bold">Tipo de plantio:  </h4>
					<div class="row">
						<div class="col-sm-2">
							<label class="radio-inline"><input type="checkbox" name="planting[]" value="arroz">Arroz</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="checkbox" name="planting[]" value="feijao">Feijão</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="checkbox"  name="planting[]" value="mandioca">Mandioca</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="checkbox" name="planting[]" value="milho">Milho</label>
						</div>
						<div class="col-sm-2">
							<label class="radio-inline"><input type="checkbox"  name="planting[]" value="algodão">Algodão</label>
						</div>
						
					</div>		
				</div>

				<div class="form-group">
					<textarea  name="description" rows="5" value="" class="form-control" placeholder="Descrição"></textarea>

				</div>

				<button class="btn btn-primary btn-lg pull-right" type="submit">Salvar cadastro</button>


			</form>
		</div>
	</div>
</div>





@stop
@endsection