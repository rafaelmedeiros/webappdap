<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PrBL Tool</title>

	<link href="/css/template.css" rel="stylesheet">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>



<nav class="navbar navbar-inverse" role="navigation">
				<div class="navbar-header logo col-sm-2">
				<span>PrBl </span><strong>Tool</strong>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="navbar-header col-sm-7">
				<ul class="nav navbar-nav menu">
			 <li ><a href="../../home">Home</a></li>
     		 <li ><a href="../../home">Share</a></li>
     		 <li ><a href="../../home">Export</a></li>
     		 <li ><a href="../../principal">My Projects</a></li>
 		
    </ul>
				</div>
					<div class="navbar-header col-sm-3 login">
				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
					<li><a href="../auth/login"><span class="glyphicon glyphicon-user"></span> Log in</a></li>
					<li><a href="../auth/register"><span class="glyphicon glyphicon-log-in"></span> Sign up</a></li>
					@else
						<li class="dropdown">
							<a href="#" id="down" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" id="down-menu" role="menu">
								<li><a id="down1" href="/auth/logout">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
				</div>
		</nav>



		<ul class="nav nav-tabs nav-justified" id="ul-menu">
			<li  id="li-menu1"><a  href="{{action('ProductController@form', $p['id'])}}" class="btn btn-warning btn-lg">Student Learning Guide</a></li>
			  <li   id="li-menu2"><a  href="{{action('TeamController@form', $p['id'])}}" class="btn btn-warning btn-lg">Manage Teams</a></li>
			  <li  id="li-menu3"><a  href="{{action('RubricController@form', $p['id'])}}" class="btn btn-warning btn-lg">Rubrics</a></li>
			  <li   id="li-menu4"><a  href="{{action('FallowUpController@form', $p['id'])}}" class="btn btn-warning btn-lg">Teams Fallow Up</a><li>
			
		</ul>
		</div>

	@yield('content')



<footer>
  
</footer>

	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
</body>
</html>










