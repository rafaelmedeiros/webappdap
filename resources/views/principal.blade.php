@extends('template')

@section('content')


<div class="container">




<!--   @if(empty($projects))
		  <div class="alert alert-danger">
		    Você não tem nenhum projeto cadastrado.
		  </div>
		@else
		<table class="table table-striped table-bordered table-hover">
			<caption>Listagem de projetos</caption>
			@foreach($projects as $p)
				<tbody>
					<tr>
						<td>{{ $p->title }}</td>
						<td> <a href="/produtos/mostra/{{ $p->id }}">Edit</a></td>
						 <td><a href="#"> Delete</a></td>
 						 <td> <a href="#">Html</a></td>
 						 <td> <a href="#">Pdf</a></td>
 						 <td> <a href="#">Facebook</a></td>
					</tr>
				</tbody>
		  	@endforeach
		</table>

		
		@endif
<a  href="{{action('ProjectController@form')}}" class="btn btn-primary pull-right">New Project</a>
 -->
	
<div id="demo">
	<h3 class="title" style="padding: 20px;">My projects	<a  href="{{action('ProjectController@form')}}" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"> </i> New Project</a></h3>
@if(empty($projects))
		  <div class="alert alert-danger">
		    Você não tem nenhum projeto cadastrado.
		  </div>
@else
<div class="table-responsive-vertical shadow-z-1">
  <!-- Table starts here -->
  <table id="table" class="table table-hover table-mc-light-blue">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th style="width:120px;">Actions</th>
          <th>Status</th>
        </tr>
      </thead>
      @foreach($projects as $p)
      <tbody>
        <tr>
          <td data-title="ID">{{ $p->id }}</td>
          <td data-title="Name">{{ $p->title }}</td>
          <td data-title="Link">
                 <a class="projecticons glyphicon glyphicon-search" href="/project/view/{{ $p->id }}"></a> 
                <a class="projecticons glyphicon glyphicon-edit" href="/project/edit/{{ $p->id }}" style="margin-left: 5px;margin-right: 5px"></a> 
                <a class="projecticons glyphicon glyphicon-erase" href="{{action('ProjectController@destroy', $p['id'])}}" onclick="return confirm('Are you sure you want to Remove?');"></a>
              
          </td>
          <td data-title="Status">{{ $p->status }}</td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
  @endif
</div>  


</div>

@endsection
