@extends('app')

@section('content')
<link href="/css/home.css" rel="stylesheet">
<div class="container" style="padding: 30px 0px;">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				

				<div class="panel-body">

				<div class="icon">
					<span class="glyphicon glyphicon-edit"></span>
				</div>
				

				<div class="text" >
								<h3 style="padding-bottom: 50px;">Monitore informações</h3>
							
				</div>
				<div style="padding-bottom: 55px;" class="buttons">
				<div class="col-md-3">
					<a href="../principal" class="btn btn-warning btn-lg" role="button">Sou um técnico</a>
				</div>
					
				<div class="col-md-6" ">
					<a href="../guestPage" class="btn btn-warning btn-lg" role="button">Sou um agricultor</a>
				</div>
				</div>
				

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
