@extends('app')

@section('content')
<link href="/css/login.css" rel="stylesheet">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>Welcome ! Please say it your email for us</h1>
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal"  role="form" method="POST" action="/guest">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-6">
								<input type="text" placeholder="Full name" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
						<label class="col-md-3 control-label"></label>
							<div class="col-md-6" >
								<input  placeholder="Username" type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-6">
								<button type="submit" class="btn btn-primary" >
									Go in
								</button>
							</div>
						</div>
					
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
