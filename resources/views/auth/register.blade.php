@extends('app')

@section('content')

<script type="text/javascript">
function fnCheckRadio(frm)
{   
     var radYes=document.getElementById("yes"); 
     if(radYes.checked)
     {          
     	frm.submit();    
        return true;

		
     }
     else
     {
        alert("Please agree Terms and condition");
        return false;
     }
   return false;
}

</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><center>Register</center></div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<center>
					<form class="form-horizontal" role="form" method="POST" action="/signUp" onsubmit="fnCheckRadio(this); return false">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
						
							
								<input type="text" placeholder="Full name" class="form-control" name="name" value="{{ old('name') }}">
							
						

						</div>

						<div class="form-group">
							
								<input type="email"  placeholder="E-mail" class="form-control" name="email" value="{{ old('email') }}">
							
						</div>

						<div class="form-group">
							
								<input type="password" placeholder="Password" class="form-control" name="password">
							
						</div>

						<div class="form-group">
							
							
								<input type="password"  placeholder="Password Confirmation" class="form-control" name="password_confirmation">
							
						</div>

						<div class="form-group">
							
						<select class="selectpicker" data-live-search="true" name="type" value="{{ old('type') }}"> 
						  <option selected disabled>Academic Institution Type</option>
						  <option>K12 School</option>
						  <option>University</option>
						  <option>Research Institute</option>
						  <option>NGO</option>
						  <option>Other</option>
						</select>
							
						</div>

						<div class="form-group">
							
								<input type="text" placeholder="Country" class="form-control" name="institutionCountry" value="{{ old('institutionCountry') }}">
							
						</div>


						<div class="form-group">
							
								<input type="text" placeholder="Academic Institution Name" class="form-control" name="institution" value="{{ old('institution') }}">
							
						</div>

						<div class="form-group">
							
								<input type="text" placeholder="Academic Institution Acronym" class="form-control" name="acronym" value="{{ old('acronym') }}">
							
						</div>

						<input type="checkbox" name="radio" id="yes"> I accept the Term of Service and Privacy Police
						<br></br>
						<div class="form-group">
							
								<button  type="submit" class="btn btn-primary">
									Create new account
								</button>
							
						</div>
					</form></center>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
